var fileLoader = document.getElementById('fileLoader'),
  loaderClassList = document.getElementById('fileLoader').classList,
  saveImage = document.getElementById('saveImage'),
  saveImageClassList = document.getElementById('saveImage').classList,
  canvasSource = document.getElementById('canvas-source'),
  canvasTarget = document.getElementById('canvas-target'),
  ctxSource = canvasSource.getContext('2d'),
  ctxTarget = canvasTarget.getContext('2d');
  
  imagesCache = {};
/**
 * Converts RGB component to HEX  
 * @param  {Integer} c Component's value
 * @return {String}   HEX value
 */
function componentToHex(c) {
  var hex = c.toString(16);
  return hex.length == 1 ? "0" + hex : hex;
}

/**
 * Converts RGB color value to HEX
 * @param  {Integer} r RED color component
 * @param  {Integer} g GREEN color component
 * @param  {Integer} b BLUE color component
 * @return {String}   HEX value (w/o leading #)
 */
function rgbToHex(r, g, b) {
  return "" + componentToHex(r) + componentToHex(g) + componentToHex(b);
}

/**
 * Calculates average color value for canvas tile
 * @param  {ImageData} piece  2d canvas context data
 * @return {String}         HEX value
 */
function getAverageColoursHEX(tile) {
  var count = 0,
    i = -4,
    length,
    rgb = {
      r: 255,
      g: 255,
      b: 255
    };

  data = tile.data;
  length = data.length;
  while ((i += 4) < length) {
    count++;
    rgb.r += data[i];
    rgb.g += data[i + 1];
    rgb.b += data[i + 2];
  }

  rgb.r = Math.floor(rgb.r / count);
  rgb.g = Math.floor(rgb.g / count);
  rgb.b = Math.floor(rgb.b / count);

  return rgbToHex(rgb.r, rgb.g, rgb.b);
}

/**
 * Fetches image from server, caching and drawing it on canvas
 * @param  {String}   color  Color's HEX value
 * @param  {CanvasRenderingContext2D}   ctx   Canvas context
 * @param  {Integer}   posX  tile X position
 * @param  {Integer}   posY  tile Y position
 * @param  {Function} cb    Callback function
 */
function loadColor(color, ctx, posX, posY, cb) {
  if (!color) return;
  if (imagesCache[color]) {
    ctx.drawImage(images[color], posX, posY);
    cb(true);
  } else {
    var img = new Image();
    img.onload = function() {
      ctx.drawImage(img, posX, posY);
      imagesCache[color] = img;
      cb(true);
    };
    img.src = 'http://localhost:8765/color/' + color;
  }
}

/**
 * splits image to tiles
 * @param  {Canvas} canvas Source canvas
 * @return {Array}        Array with tiles data
 */
function getTiles(canvas, tilesX, tilesY, tileWidth, tileHeight) {
  var ctx = canvas.getContext('2d'),
      result = [];
  for (var i = 0; i < tilesY; i++) {
    for (var j = 0; j < tilesX; j++) {
      result.push(
          getAverageColoursHEX(
            ctx.getImageData(  
              j * tileWidth, 
              i * tileHeight, 
              tileWidth, 
              tileHeight
            )
          )
      );
    }
  }
  return result;  
}

/**
 * Converts flat tiles array to 2D array
 * @param  {Array} tiles  Source flat tiles array
 * @param  {Integer} sizeY Rows quantity
 * @return {Array}       2D array or tiles colors
 */
function getTilesMatrix(tiles, sizeY) {
  var rows = [],
    rowsCounter = -1;
  for (var i = 0; i < tiles.length; i++) {
    if (i % sizeY === 0) {
      rows.push([]);
      rowsCounter++;
    }
    rows[rowsCounter].push(tiles[i]);
  }
  return rows;
}

/**
 * Draws single row of tiles
 * @param  {Array} tiles     Array of tiles colors
 * @param  {CanvasRenderingContext2D} ctx       Canvas context
 * @param  {Integer} rowNumber Target row number
 * @param  {function} cb callback function
 */
function drawRow(tiles, ctx, rowNumber, cb) {
  var _canvas = document.createElement('canvas');
  var _context = _canvas.getContext('2d');
  _canvas.width = tiles.length * TILE_WIDTH;
  _canvas.height = TILE_HEIGHT;
  var loadedTilesCounter = 0;
  for (var i = 0; i < tiles.length; i++) {
    loadColor(tiles[i], _context, i * TILE_WIDTH, 0, function(status) {
      loadedTilesCounter++;
      if (loadedTilesCounter === tiles.length) {
        ctx.drawImage(_canvas, 0, rowNumber * TILE_WIDTH);
        cb(rowNumber);
      }
    });
  }
}

/**
 * Draws whole mosaic
 * @param  {Array} tiles  Array of tiles colors
 * @param  {CanvasRenderingContext2D} ctx   Canvas context
 */
function drawMosaic(tiles, ctx) {
  for (var i = 0; i < tiles.length; i++) {
    drawRow(tiles[i], ctx, i, function(lastRow){
      if (lastRow === tiles.length-1) {
        loaderClassList.remove('loading','toggled');
        loaderClassList.add('loaded');
        saveImageClassList.add('visible');
      } 
    });
  }
}

/**
 * handles source image onLoad event
 * @param  {Image}  img source image
 */
function handleImageLoad(img) {
  canvasSource.width = img.width;
  canvasSource.height = img.height;
  canvasTarget.width = img.width;
  canvasTarget.height = img.height;
  ctxSource.drawImage(img, 0, 0);
  var tileWidth = TILE_WIDTH,
    tileHeight = TILE_WIDTH,
    tilesX = canvasSource.width / tileWidth,
    tilesY = canvasSource.height / tileHeight,
    totalTiles = tilesX * tilesY,
    tileData = getTiles(canvasSource, tilesX, tilesY, tileWidth, tileHeight);
  var matrix = getTilesMatrix(tileData, Math.round(tilesX), Math.round(tilesY));
  drawMosaic(matrix, ctxTarget);

}

/**
 * reset canvases and cache
 */
function resetAll() {

  imagesCache = {};
  
  ctxSource.clearRect(0, 0, canvasSource.width, canvasSource.height);
  ctxTarget.clearRect(0, 0, canvasTarget.width, canvasTarget.height);

  saveImageClassList.remove('visible');
  loaderClassList.remove('loaded');
  loaderClassList.add('toggled','loading');

}

/**
 * Handles file picker onLoad event 
 * @param  {Event} e  Source event 
 */
function handleImage(e) {

  resetAll();
  
  var reader = new FileReader();
  reader.onload = function(event) {
    var img = new Image();
    img.onload = function() {
      handleImageLoad(img);
    };
    img.src = event.target.result;
  };
  reader.readAsDataURL(e.target.files[0]);
}

// The magic is starting here
fileLoader.addEventListener('change', handleImage, false);


/**
 * Handles image save action 
 */
function handleImageSave() {
  window.open(canvasTarget.toDataURL('image/png').replace(/^data:image\/[^;]/, 'data:application/octet-stream'));
}

saveImage.addEventListener('click', handleImageSave, false);
