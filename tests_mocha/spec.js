describe("Testing MOSAIC", function() {
  

    before(function() {
      var canvas = document.createElement('canvas');
      var ctx = canvas.getContext('2d');
      canvas.setAttribute('id','testing');
      canvas.setAttribute('style','visibility:hidden');
      canvas.width = 100;
      canvas.height = 100;
      ctx.fillStyle = 'rgb(255,0,0)';
      ctx.fillRect(0,0,50,100);
      ctx.fillStyle = 'rgb(0,0,0)';
      ctx.fillRect(50,0,100,100);
      document.body.appendChild(canvas);
  });

  it("Converts RGB to HEX", function() {
    var res = rgbToHex(255,255,255);
    expect(res).to.be.equal('ffffff');
  });
  
  it("Calculates average colors HEX for canvas tile", function() {
      var canvas = document.getElementById('testing'),
          ctx = canvas.getContext('2d'),
          tile = ctx.getImageData(0,0,100,100),
          res = getAverageColoursHEX(tile);
      expect(res).to.be.equal('7f0000');
  });


 it("splits canvas to tiles and gets colors correctly", function() {
      var canvas = document.getElementById('testing'),
          res = getTiles(canvas,2,2,50,50);
      expect(res.length).to.be.equal(4);
      expect(res[0]).to.be.equal('ff0000');
      expect(res[3]).to.be.equal('000000');
  });


});